FROM node:8

WORKDIR /usr/src/app
ADD . /usr/src/app

RUN npm install
RUN npm build

EXPOSE 8080

CMD ["npm", "start"]
