const mongoose = require('mongoose');

const OrderSchema = new mongoose.Schema({
  origin: {
    type: Array,
    required: true
  },
  destination: {
    type: Array,
    required: true,
  },
  distance:{
      type:String
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
}, {
  versionKey: false
});


module.exports = mongoose.model('Order', OrderSchema);
