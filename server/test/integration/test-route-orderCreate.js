var supertest = require('supertest'),
app = require('../../index');

exports.addition_should_accept_arrays = function(done){
  supertest(app)
  .post('/order')
  .send({origin:[89.32,34.43],destination:[23.43,23.32]})
  .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c')
  .expect(200,done)
  .end(done);
};

// exports.addition_should_reject_strings = function(done){
//   supertest(app)
//   .get('/add?a=string&b=2')
//   .expect(422)
//   .end(done);
// };